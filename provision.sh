#!/bin/bash

mkdir -p /root/.ssh
cp /vagrant/keys/id_rsa* /root/.ssh/
cp /vagrant/keys/id_rsa.pub /root/.ssh/authorized_keys

if [ -n "$(which yum)" ]; then
  yum update -y
  yum install -y vim wget curl openldap openldap-clients openldap-servers
fi

if [ -n "$(which apt-get)" ]; then
  apt-get update
  #apt-get dist-upgrade -y
cat << EOF | sudo debconf-set-selections
slapd slapd/internal/adminpw password 4linux
slapd slapd/internal/generated_adminpw password 4linux
slapd slapd/password2 password 4linux
slapd slapd/password1 password 4linux
slapd slapd/dump_database_destdir string /var/backups/slapd-VERSION
slapd slapd/domain string example.com
slapd shared/organization string Example S/A
slapd slapd/backend string MDB
slapd slapd/purge_database boolean true
slapd slapd/move_old_database boolean true
slapd slapd/allow_ldap_v2 boolean false
slapd slapd/no_configuration boolean false
slapd slapd/dump_database string when needed
EOF
  DEBIAN_FRONTEND=noninteractive apt-get install -y vim wget curl slapd ldap-utils
fi

if [ -n "$(which zypper)" ]; then
  zypper install -y vim wget curl openldap2 openldap2-client
fi
