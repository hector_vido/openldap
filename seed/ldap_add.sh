#!/bin/bash

I=100001
while read LINE; do
	CN=$(echo $LINE | cut -d, -f1)
	SN=$(echo $LINE | cut -d, -f2)
  _UID=$(echo "$CN.$SN" | tr [:upper:] [:lower:] | tr -d - | tr -s ' ' | tr ' ' -)
	ldapadd -D cn=admin,dc=example,dc=com -w 4linux <<EOF
dn: uid=$_UID,ou=users,dc=example,dc=com
objectClass: inetOrgPerson
objectClass: posixAccount
cn: $CN
sn: $SN
givenName: $CN $SN
mail: $_UID@example.com
uid: $_UID
uidNumber: $I
gidNumber: $I
loginShell: /bin/bash
homeDirectory: /srv/home/$_UID
userPassword: $(slappasswd -n -s 4linux)
EOF
I=$(($I+1))
done < users.csv
